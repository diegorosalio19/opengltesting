#include "Application.h"
#include "InputHandler.h"
#include "../world/generator/FlatGenerator.h"
#include "../world/generator/NormalGenerator.h"
#include <random>
#include <chrono>

Application::Application()
{
	window = initGLFWLibrary();
	glfwGetWindowSize(window, &screenSize.width, &screenSize.height);
	glViewport(0, 0, screenSize.width, screenSize.height);
	InputHandler::registerHandler(window);
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
}

GLFWwindow *Application::initGLFWLibrary()
{
	GLFWwindow *window;
	if (!glfwInit())
		return nullptr;
	glfwWindowHint(GLFW_MAXIMIZED, GLFW_TRUE);
	window = glfwCreateWindow(640, 480, "AmongUs", nullptr, nullptr);
	glfwSetInputMode(window, GLFW_CURSOR, GLFW_CURSOR_DISABLED);
	if (!window) {
		glfwTerminate();
		return nullptr;
	}
	glfwMakeContextCurrent(window);
	gladLoadGL();
	return window;
}

Application::~Application()
{
	glfwTerminate();
}

void Application::show()
{
	Shader crosshairShader { "shader/Crosshair.vert", "shader/OneColor.frag" };
	Shader basicShader { "shader/ChunkShader.vert", "shader/ChunkShader.frag" };
	Crosshair crosshair { screenSize, crosshairShader };
	GUI gui { crosshair };


	auto *camera = new Camera { glm::vec3 { 0, 32.f, 0 }};
	InputHandler::setCamera(camera);
	world = new WorldComponents::World { camera };
	FlatGenerator normalGenerator { *world, 16 };

	RenderEngine renderEngine { basicShader, screenSize };
	ChunkBlockSelectorManager chunkBlockSelectorManager { *world };
	Mobs::HumanLikeMob humanLikeMob{glm::vec3 {0, 33, 0}};
	Mobs::HumanLikeMob steve{glm::vec3 {5, 33, 0}};

	while (!glfwWindowShouldClose(window)) {
		float elapsedTime = getElapsedTime();
		setFpsTitle(elapsedTime);
		InputHandler::setElapsedTime(&elapsedTime);
		InputHandler::handleKeyEvent(window);

		for (int i = -12; i < 13; ++i) {
			for (int j = -12; j < 13; ++j) {
                normalGenerator.generate(glm::ivec2 { i + ((int) world->getCamera()->getPos().x) / 16,
				                                    j + ((int) world->getCamera()->getPos().z) / 16 });
			}
		}

//		steve.setPosition(glm::vec3{world->getCamera()->getPos().x - 0.5f, world->getCamera()->getPos().y - 1.75f, world->getCamera()->getPos().z - 0.25});

		RenderEngine::clear();
		renderEngine.drawWorld(*world);
		renderEngine.drawMob(humanLikeMob, *world);
		renderEngine.drawMob(steve, *world);
		RenderEngine::drawGUI(gui);

		try {
			auto selectedPosition = chunkBlockSelectorManager.getSelectedBlockPosition(world->getCamera()->getPos(),
			                                                                           world->getCamera()->getFront());
			renderEngine.drawWireFramedCube(selectedPosition, 1, *world);
			if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_LEFT) == GLFW_PRESS) {
				world->removeBlock(selectedPosition);
			}
			if (glfwGetMouseButton(window, GLFW_MOUSE_BUTTON_RIGHT) == GLFW_PRESS) {
				auto faceDirection = ChunkBlockSelectorManager::getSelectedFaceDirection(
						world->getCamera()->getFront());
				try {
					world->setBlock(new WorldComponents::Block { WorldComponents::BlockType::DIRT },
					                selectedPosition + faceDirection);
				} catch (WorldComponents::LimitException &limitException) {
					std::cerr << limitException.errorMessage << std::endl;
				}
			}

		} catch (ChunkBlockSelectorManager::NothingSelectedException &nothingSelectedException) {}
		glfwSwapBuffers(window);
		glfwPollEvents();
	}
}


float Application::getElapsedTime()
{
	static float lastTime;
	auto currentTime = (float) glfwGetTime();
	float elapsedTime = currentTime - lastTime;
	lastTime = currentTime;
	return elapsedTime;
}

ScreenSize Application::getScreenSize() const
{
	return screenSize;
}

void Application::setFpsTitle(float time)
{
	static auto lastTime = (float) glfwGetTime();
	auto now = (float) glfwGetTime();
	if (now - lastTime > 0.4) {
		lastTime = now;
		glfwSetWindowTitle(window, (std::to_string(1 / time) + "fps @{x: " +
		                            std::to_string(world->getCamera()->getPos().x) + " y: " +
		                            std::to_string(world->getCamera()->getPos().y) + " z: " +
		                            std::to_string(world->getCamera()->getPos().z) + "}").c_str());

	}
}
