#pragma once

#include <vector>
#include "../util.h"

#include "../shader/Shader.h"
#include "../world/World.h"
#include "GLAD/glad.h"
#include "GLFW/glfw3.h"
#include "GLM/glm.hpp"
#include "GLM/gtc/matrix_transform.hpp"
#include "../renderer/RenderEngine.h"
#include <memory>

class Application
{
public:
	Application();

	~Application();

	void show();

	[[nodiscard]] ScreenSize getScreenSize() const;

private:
	ScreenSize screenSize {};
	GLFWwindow *window;

private:
	WorldComponents::World *world;

	static float getElapsedTime();

	static GLFWwindow *initGLFWLibrary();

	void setFpsTitle(float time);
};
