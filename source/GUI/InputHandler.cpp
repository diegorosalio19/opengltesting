#include "InputHandler.h"

namespace InputHandler
{
	Camera *_camera;
	const float *_deltaTime;
	bool _firstMouse { true };
	bool _debugBooleanValue { false };

	void handleKeyEvent(GLFWwindow *window)
	{
		if (!_camera) return;
		if (!_deltaTime) return;
		if (glfwGetKey(window, GLFW_KEY_W) == GLFW_PRESS) {
			_camera->handleKeyboard(CameraMovement::FORWARD, *_deltaTime);
		}
		if (glfwGetKey(window, GLFW_KEY_S) == GLFW_PRESS) {
			_camera->handleKeyboard(CameraMovement::BACKWARD, *_deltaTime);
		}
		if (glfwGetKey(window, GLFW_KEY_A) == GLFW_PRESS) {
			_camera->handleKeyboard(CameraMovement::LEFT, *_deltaTime);
		}
		if (glfwGetKey(window, GLFW_KEY_D) == GLFW_PRESS) {
			_camera->handleKeyboard(CameraMovement::RIGHT, *_deltaTime);
		}
		if (glfwGetKey(window, GLFW_KEY_SPACE) == GLFW_PRESS) {
			_camera->handleKeyboard(CameraMovement::UP, *_deltaTime);
		}
		if (glfwGetKey(window, GLFW_KEY_LEFT_SHIFT) == GLFW_PRESS) {
			_camera->handleKeyboard(CameraMovement::DOWN, *_deltaTime);
		}
		if (glfwGetKey(window, GLFW_KEY_R) == GLFW_PRESS)
			_camera->setSpeed(20);
		if (glfwGetKey(window, GLFW_KEY_F) == GLFW_PRESS)
			_camera->setSpeed(4.5f);
		if (glfwGetKey(window, GLFW_KEY_V) == GLFW_PRESS)
			_debugBooleanValue = true;
		if (glfwGetKey(window, GLFW_KEY_B) == GLFW_PRESS)
			_debugBooleanValue = false;
		if (glfwGetKey(window, GLFW_KEY_T) == GLFW_PRESS)
			glPolygonMode(GL_FRONT_AND_BACK, GL_LINE);
		if (glfwGetKey(window, GLFW_KEY_Y) == GLFW_PRESS)
			glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	}

	void mouseCallback(GLFWwindow *window, double xPos, double yPos)
	{
		if (!_camera) return;
		static float lastX = 400, lastY = 300;
		if (_firstMouse) {
			lastX = (float) xPos;
			lastY = (float) yPos;
			_firstMouse = false;
		}
		float xOffset = (float) xPos - lastX;
		float yOffset = lastY - (float) yPos;
		lastX = (float) xPos;
		lastY = (float) yPos;
		_camera->handleMouseMovement(xOffset, yOffset);
	}

	void scrollCallback(GLFWwindow *window, double xOffset, double yOffset)
	{
		if (!_camera) return;
		_camera->handleMouseScroll((float) yOffset);
	}

	void framebufferSizeCallback(GLFWwindow *window, int width, int height)
	{
		glViewport(0, 0, width, height);
	}

	void registerHandler(GLFWwindow *window)
	{
		glfwSetCursorPosCallback(window, mouseCallback);
		glfwSetScrollCallback(window, scrollCallback);
		glfwSetFramebufferSizeCallback(window, framebufferSizeCallback);
	}

	void setCamera(Camera *camera)
	{
		_camera = camera;
	}

	void setElapsedTime(const float *elapsedTime)
	{
		_deltaTime = elapsedTime;
	}

	bool getDebugBooleanValue()
	{
		return _debugBooleanValue;
	}

	void setDebugBooleanValue(bool value)
	{
		_debugBooleanValue = value;
	}

}