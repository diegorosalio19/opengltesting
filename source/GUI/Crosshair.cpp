#include "Crosshair.h"

Crosshair::Crosshair(const ScreenSize aScreenSize, Shader &shader) : screenSize{aScreenSize}, shader{shader}
{
	glGenVertexArrays(1, &VAO);
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	glBindVertexArray(VAO);

	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, sizeof(vertices), vertices, GL_STATIC_DRAW);
	glVertexAttribPointer(0, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), nullptr);
	glEnableVertexAttribArray(0);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(indices), indices, GL_STATIC_DRAW);

	glBindVertexArray(0);
}

Crosshair::~Crosshair()
{
	glDeleteVertexArrays(1, &VAO);
	glDeleteBuffers(1, &VBO);
	glDeleteBuffers(1, &EBO);
}

void Crosshair::draw() const
{
	glm::mat4 view {1.f};
	glm::mat4 model { 1.f };

	auto widthFactor = (float) screenSize.height / (float) screenSize.width;
	view = glm::scale(view, glm::vec3{widthFactor, 1, 1});
	model = glm::scale(model, glm::vec3 { 0.02, 0.02, 1 });

	shader.use();
	shader.setMat4f("uProjection", view);
	shader.setMat4f("uModel", model);
	shader.setVec3("uColor", glm::vec3 { 1.f, 1.f, 1.f });

	glBindVertexArray(VAO);
	glDrawElements(GL_TRIANGLES, 12, GL_UNSIGNED_INT, nullptr);
	glBindVertexArray(0);
}


