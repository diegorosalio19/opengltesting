#pragma once

#include "GLAD/glad.h"
#include "GLFW/glfw3.h"
#include "../Camera.h"
#include <iostream>
#include "../world/chunk/ChunkSection.h"
#include <vector>

namespace InputHandler
{
	void mouseCallback(GLFWwindow* window, double xPos, double yPos);
	void scrollCallback(GLFWwindow* window, double xOffset, double yOffset);
	void framebufferSizeCallback(GLFWwindow* window, int width, int height);

	void registerHandler(GLFWwindow* window);
	void setCamera(Camera* camera);
	void setElapsedTime(const float* elapsedTime);
	void handleKeyEvent(GLFWwindow* window);

	bool getDebugBooleanValue();
	void setDebugBooleanValue(bool value);

};
