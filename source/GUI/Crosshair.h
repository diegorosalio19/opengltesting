#pragma once

#include "../util.h"

class Crosshair
{
public:
	explicit Crosshair(ScreenSize aScreenSize, Shader &shader);
	~Crosshair();

public:
	void draw() const;

private:
	ScreenSize screenSize;
	Shader shader;
	float width {0.2f};
	float vertices[16] = {
			-width/2,  1.f, //0
			width/2,  1.f, //1
			1.f,  width/2, //2
			1.f, -width/2, //3
			width/2, -1.f, //4
			-width/2, -1.f, //5
			-1.f, -width/2, //6
			-1.f,  width/2, //7
	};
	unsigned int indices[12] = {
			0, 4, 1,
			0, 5, 4,
			7, 6, 3,
			7, 3, 2
	};
	unsigned int VBO{}, VAO{}, EBO{};

};