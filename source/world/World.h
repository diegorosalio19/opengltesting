#pragma once

#include "../Camera.h"
#include "Light.h"
#include "../shader/Shader.h"
#include "Block.h"
#include "chunk/Chunk.h"
#include "../util.h"
#include <vector>
#include <map>

namespace WorldComponents
{

	struct Position2DComparator {
		bool operator() (const glm::ivec2& first, const glm::ivec2& second) const
		{
			if(first.x < second.x) return true;
			if(first.x > second.x) return false;
			if(first.y < second.y) return true;
			return false;
		}
	};

	class World
	{
	public:
		explicit World(Camera *camera);

		Camera *getCamera();
		Block* getBlock(ivec3 position);
		void removeBlock(ivec3 position);
		void setBlock(Block *block, ivec3 aPosition);

        [[nodiscard]] vec3 getPlayerPosition() const;

		[[nodiscard]] glm::mat4 getProjViewMatrix(const ScreenSize &screenSize) const;

	private:
		Camera *camera;

	public:
		std::vector<PointLight> pointLights;
		std::vector<SpotLight> spotLights;
		DirectionLight directionLight {};
		std::map<glm::ivec2, Chunk*, Position2DComparator> chunks;
	};
}
