#pragma once

#include "GLM/glm.hpp"
#include "../TextureLoader.h"
#include <set>

namespace WorldComponents
{
	enum class BlockType
	{
		DIRT,
		BARREL
	};

	class Block
	{
	public:
		explicit Block(BlockType blockType);

		TextureLoader::Texture getTexture();

		[[nodiscard]] bool isOpaque() const;

	private:
		BlockType blockType;
	};
}