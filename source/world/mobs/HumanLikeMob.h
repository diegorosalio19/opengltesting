#pragma once
#include "MobConstants.h"

namespace Mobs
{
	class HumanLikeMob
	{
	public:
		explicit HumanLikeMob(const vec3 &position);

		void setPosition(const vec3 &position);

		[[nodiscard]] const vec3 &getPosition() const;

	private:
		vec3 position;
	};
}
