#include "HumanLikeMob.h"

Mobs::HumanLikeMob::HumanLikeMob(const glm::vec3 &position) : position(position)
{}

const glm::vec3 &Mobs::HumanLikeMob::getPosition() const
{
	return position;
}

void Mobs::HumanLikeMob::setPosition(const glm::vec3 &position)
{
	HumanLikeMob::position = position;
}
