#include "ChunkMeshBuilder.h"


namespace WorldComponents
{
	struct AdjacentPositions
	{
		void update(const ivec3 &position)
		{
			up = { position.x, position.y + 1, position.z };
			down = { position.x, position.y - 1, position.z };
			left = { position.x - 1, position.y, position.z };
			right = { position.x + 1, position.y, position.z };
			front = { position.x, position.y, position.z + 1 };
			back = { position.x, position.y, position.z - 1 };
		}

		ivec3 up;
		ivec3 down;
		ivec3 left;
		ivec3 right;
		ivec3 front;
		ivec3 back;
	};

	ChunkMeshBuilder::ChunkMeshBuilder(const ChunkSection &chunkSection)
			: chunkSection(
			chunkSection)
	{}

	ChunkMesh ChunkMeshBuilder::buildMesh()
	{
		currentMesh = ChunkMesh {};
		for (int y = 0; y < CHUNK_SIZE; ++y) {
			for (int x = 0; x < CHUNK_SIZE; ++x) {
				for (int z = 0; z < CHUNK_SIZE; ++z) {
					Block *currentBlock = chunkSection.getBlock(ivec3 { x, y, z });
					if (!currentBlock) continue;
					AdjacentPositions adjacentPositions {};
					adjacentPositions.update(ivec3 { x, y, z });
					tryAddFaceToMesh(topFace, currentBlock->getTexture(), topFaceTextureCoords, ivec3 { x, y, z },
					                 adjacentPositions.up);
					tryAddFaceToMesh(bottomFace, currentBlock->getTexture(), bottomFaceTextureCoords, ivec3 { x, y, z },
					                 adjacentPositions.down);
					tryAddFaceToMesh(rightFace, currentBlock->getTexture(), rightFaceTextureCoords, ivec3 { x, y, z },
					                 adjacentPositions.right);
					tryAddFaceToMesh(leftFace, currentBlock->getTexture(), leftFaceTextureCoords, ivec3 { x, y, z },
					                 adjacentPositions.left);
					tryAddFaceToMesh(backFace, currentBlock->getTexture(), backFaceTextureCoords, ivec3 { x, y, z },
					                 adjacentPositions.back);
					tryAddFaceToMesh(frontFace, currentBlock->getTexture(), frontFaceTextureCoords, ivec3 { x, y, z },
					                 adjacentPositions.front);
				}
			}
		}
		return currentMesh;
	}

	void ChunkMeshBuilder::tryAddFaceToMesh(const std::array<float, 12> &face, const TextureLoader::Texture &texture,
	                                        const std::array<float, 8> &textureCoords, const ivec3 &blockPosition,
	                                        const ivec3 &adjBlockPosition)
	{
		try {
			Block *adjBlock = chunkSection.getBlock(adjBlockPosition);
			if (adjBlock == nullptr || shouldMakeFace(*adjBlock)) {
				currentMesh.addFace(face, texture, textureCoords, chunkSection.getPosition(), blockPosition);
			}
		} catch (LimitException &limitException) {
			currentMesh.addFace(face, texture, textureCoords, chunkSection.getPosition(), blockPosition);
		}
	}

	bool ChunkMeshBuilder::shouldMakeFace(const Block &adjBlock)
	{
		return !adjBlock.isOpaque();
	}


}
