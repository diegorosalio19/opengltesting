#pragma once

#include <array>
#include "../Block.h"
#include "../WorldConstants.h"
#include "ChunkMesh.h"

namespace WorldComponents
{
	class  ChunkSection
	{
	public:
		class Layer
		{
		public:
			void update(Block block)
			{
				if (block.isOpaque()) solidBlockCount++;
				else solidBlockCount--;
			}

			void removeBlock()
			{
				solidBlockCount--;
			}

			[[nodiscard]] bool isAllSolid() const
			{
				return solidBlockCount == CHUNK_AREA;
			}

		private:
			int solidBlockCount { 0 };
		};

	public:
		ChunkSection();

		explicit ChunkSection(const ivec3 &position);

		[[nodiscard]] Block *getBlock(ivec3 position) const;

		[[nodiscard]] const ivec3 &getPosition() const;

		void setPosition(const ivec3 &position);

		void setBlock(Block *block, ivec3 aPosition);

		void removeBlock(ivec3 position);

		[[nodiscard]] const ChunkMesh & getMesh() const;

		[[nodiscard]] const Layer &getLayer(int y) const;

		void invalidateBufferedMesh();

	private:
		std::array<Block*, CHUNK_SECTION_VOLUME> blocks {};
		std::array<Layer, CHUNK_SIZE> layers {};
		ivec3 position;
		mutable ChunkMesh chunkMesh;
		mutable bool chunkMeshValid;
	};
}
