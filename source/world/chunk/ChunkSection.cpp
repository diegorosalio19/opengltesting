#include "ChunkSection.h"
#include "ChunkMeshBuilder.h"

namespace WorldComponents
{
	inline int PositionToIndex(ivec3 aPosition)
	{ return aPosition.x * CHUNK_SIZE + aPosition.y * CHUNK_AREA + aPosition.z; }

	inline bool isValidPosition(ivec3 aPosition)
	{
		return aPosition.x >= 0 && aPosition.x < CHUNK_SIZE &&
		       aPosition.y >= 0 && aPosition.y < CHUNK_SIZE &&
		       aPosition.z >= 0 && aPosition.z < CHUNK_SIZE;
	}

	ChunkSection::ChunkSection()
	= default;

	ChunkSection::ChunkSection(const ivec3 &position) : position { position }
	{}

	Block *ChunkSection::getBlock(ivec3 aPosition) const
	{
		if (!isValidPosition(aPosition)) throw LimitException { "Getting block: invalid position", aPosition };
		return blocks[PositionToIndex(aPosition)];
	}

	const glm::ivec3 &ChunkSection::getPosition() const
	{
		return position;
	}

	void ChunkSection::setBlock(Block *block, ivec3 aPosition)
	{
		if (!isValidPosition(aPosition)) throw LimitException { "Setting block: invalid position", aPosition };
		if (getBlock(aPosition)) return;
		chunkMeshValid = false;
		int index = PositionToIndex(aPosition);
		layers[aPosition.y].update(*block);
		if (!blocks[index]) delete blocks[index];
		blocks[index] = block;
	}

	void ChunkSection::removeBlock(ivec3 aPosition)
	{
		if (!isValidPosition(aPosition)) throw LimitException { "Removing block: invalid position", aPosition };
		chunkMeshValid = false;
		layers[aPosition.y].removeBlock();
		auto block = blocks[PositionToIndex(aPosition)];
		delete block;

		blocks[PositionToIndex(aPosition)] = nullptr;
	}

	const ChunkMesh & ChunkSection::getMesh()  const
	{
		if (chunkMeshValid) return chunkMesh;
		chunkMeshValid = true;
		ChunkMeshBuilder meshBuilder { *this };
		chunkMesh = meshBuilder.buildMesh();
		return chunkMesh;
	}

	const ChunkSection::Layer &ChunkSection::getLayer(int y) const
	{
		if (y < 0 || y >= CHUNK_SIZE) throw LimitException { "Getting layer: invalid y", ivec3 { 0, y, 0 }};
		return layers[y];
	}

	void ChunkSection::invalidateBufferedMesh()
	{
		chunkMeshValid = false;
	}

	void ChunkSection::setPosition(const ivec3 &aPosition)
	{
		ChunkSection::position = aPosition;
	}

}
