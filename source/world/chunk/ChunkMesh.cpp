#include "ChunkMesh.h"

namespace WorldComponents
{
	ChunkMesh::ChunkMesh()
	= default;

	ChunkMesh::ChunkMesh(const ChunkMesh &other)
	{
		VAO = other.VAO;
		VBO = other.VBO;
		EBO = other.EBO;
		VAOValid = other.VAOValid;
		vertexPositions = other.vertexPositions;
		vertexNormals = other.vertexNormals;
		indices = other.indices;
		currentIndex = other.currentIndex;
		textureCoords = other.textureCoords;
		offsetPositions = other.offsetPositions;
	}

	ChunkMesh::~ChunkMesh()
	= default;

	void ChunkMesh::addFace(const std::array<float, 12> &aBlockFace,
	                        const TextureLoader::Texture &aTexture,
	                        const std::array<float, 8> &aTextureCoords,
	                        const glm::ivec3 &aChunkPosition,
	                        const glm::ivec3 &aBlockPosition)
	{
		for (int i = 0, index = 0; i < 4; ++i) {
			vertexPositions.push_back(
					aBlockFace[index++] + (float) aBlockPosition.x + (float) aChunkPosition.x * CHUNK_SIZE);
			vertexPositions.push_back(
					aBlockFace[index++] + (float) aBlockPosition.y + (float) aChunkPosition.y * CHUNK_SIZE);
			vertexPositions.push_back(
					aBlockFace[index++] + (float) aBlockPosition.z + (float) aChunkPosition.z * CHUNK_SIZE);
		}

		WorldComponents::Direction faceDirection = GetDirectionFromFaceCoords(aBlockFace);

		for (int i = 0; i < 4; ++i) {
			vertexNormals.push_back(faceDirection.x);
			vertexNormals.push_back(faceDirection.y);
			vertexNormals.push_back(faceDirection.z);
		}

		offsetPositions.push_back(aBlockPosition + aChunkPosition);

		indices.insert(indices.begin(), {
				currentIndex, currentIndex + 2, currentIndex + 1,
				currentIndex, currentIndex + 3, currentIndex + 2
		});
		currentIndex += 4;
		textureCoords.emplace_back(aTexture, aTextureCoords);
		VAOValid = false;
	}

	const std::vector<float> &ChunkMesh::getVertexPositions() const
	{
		return vertexPositions;
	}

	const std::vector<unsigned int> &ChunkMesh::getIndices() const
	{
		return indices;
	}

	const std::vector<std::pair<TextureLoader::Texture, std::array<float, 8>>> &ChunkMesh::getTextureCoords() const
	{
		return textureCoords;
	}

	unsigned int ChunkMesh::getVAO() const
	{
		if (VAOValid) return VAO;
		destroyVAO();
		generateVAO();
		return VAO;
	}

	void ChunkMesh::generateVAO() const
	{
		std::vector<float> texturePositions;
		for (auto &element: textureCoords) {
			texturePositions.insert(texturePositions.end(), element.second.begin(), element.second.end());
		}
		glGenBuffers(1, &VBO);
		glGenVertexArrays(1, &VAO);
		glBindVertexArray(VAO);
		glBindBuffer(GL_ARRAY_BUFFER, VBO);
		glBufferData(GL_ARRAY_BUFFER, vertexPositions.size() * sizeof(float) + texturePositions.size() * sizeof(float) +
		                              vertexNormals.size() * sizeof(float), nullptr, GL_DYNAMIC_DRAW);
		glBufferSubData(GL_ARRAY_BUFFER, 0, vertexPositions.size() * sizeof(float), vertexPositions.data());
		glBufferSubData(GL_ARRAY_BUFFER, vertexPositions.size() * sizeof(float),
		                texturePositions.size() * sizeof(float), texturePositions.data());
		glBufferSubData(GL_ARRAY_BUFFER,
		                vertexPositions.size() * sizeof(float) + texturePositions.size() * sizeof(float),
		                vertexNormals.size() * sizeof(float), vertexNormals.data());
		glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
		glEnableVertexAttribArray(0);
		glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float),
		                      (void *) (vertexPositions.size() * sizeof(float)));
		glEnableVertexAttribArray(1);
		glVertexAttribPointer(2, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float),
		                      (void *) (vertexPositions.size() * sizeof(float) +
		                                texturePositions.size() * sizeof(float)));
		glEnableVertexAttribArray(2);
		glGenBuffers(1, &EBO);
		glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
		glBufferData(GL_ELEMENT_ARRAY_BUFFER, indices.size() * sizeof(unsigned int), indices.data(), GL_DYNAMIC_DRAW);
		glBindVertexArray(0);
		VAOValid = true;
	}

	void ChunkMesh::destroyVAO() const
	{
		glDeleteBuffers(1, &VBO);
		glDeleteBuffers(1, &EBO);
		glDeleteVertexArrays(1, &VAO);
	}

	const std::vector<ivec3> &ChunkMesh::getOffsetPositions() const
	{
		return offsetPositions;
	}
}
