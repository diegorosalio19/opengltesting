#pragma once

#include <vector>
#include <array>
#include "../WorldConstants.h"
#include "ChunkSection.h"

namespace WorldComponents
{
	class Chunk
	{
	public:
		Chunk(std::array<ChunkSection, 16> sections, const glm::ivec2 &position);

		[[nodiscard]] const std::array<ChunkSection, 16> &getSections() const;
		[[nodiscard]] Block *getBlock(glm::ivec3 position) const;

		void removeBlock(ivec3 position);
		void setBlock(Block *block, ivec3 aPosition);

	private:
		std::array<ChunkSection, 16> sections;
		glm::ivec2 position;
	};
}
