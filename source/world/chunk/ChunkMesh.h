#pragma once

#include <array>
#include <vector>
#include "../WorldConstants.h"
#include "../../TextureLoader.h"
#include "../../util.h"

namespace WorldComponents
{
	class ChunkMesh
	{
	public:
		ChunkMesh();

		ChunkMesh(const ChunkMesh &);

		virtual ~ChunkMesh();

		void addFace(const std::array<float, 12> &blockFace,
		             const TextureLoader::Texture &texture,
		             const std::array<float, 8> &textureCoords,
		             const ivec3 &chunkPosition,
		             const ivec3 &blockPosition);

		[[nodiscard]] const std::vector<float> &getVertexPositions() const;

		[[nodiscard]] const std::vector<unsigned int> &getIndices() const;

		[[nodiscard]] const std::vector<std::pair<TextureLoader::Texture, std::array<float, 8>>> &

		getTextureCoords() const;

		[[nodiscard]] const std::vector<ivec3> &getOffsetPositions() const;

		[[nodiscard]] unsigned int getVAO() const;

	private:
		mutable unsigned int VBO {}, VAO {}, EBO {};
		mutable bool VAOValid;
		std::vector<float> vertexPositions;
		std::vector<float> vertexNormals;
		std::vector<unsigned int> indices;
		unsigned int currentIndex { 0 };
		std::vector<std::pair<TextureLoader::Texture, std::array<float, 8>>> textureCoords;
		std::vector<ivec3> offsetPositions;

		void generateVAO() const;

		void destroyVAO() const;
	};
}
