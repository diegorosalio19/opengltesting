#include "Chunk.h"

#include <utility>

namespace WorldComponents
{
	Chunk::Chunk(std::array<ChunkSection, 16> sections, const glm::ivec2 &position) : sections(std::move(sections)),
	                                                                                  position(position)
	{}

	const std::array<ChunkSection, 16> &Chunk::getSections() const
	{
		return sections;
	}

	Block *Chunk::getBlock(glm::ivec3 aPosition) const
	{
		if (aPosition.y < 0) return nullptr;
		if (aPosition.y >= 256) throw LimitException { "Height Limit", aPosition };
		return sections.at(aPosition.y / 16).getBlock(glm::ivec3 { aPosition.x, aPosition.y % 16, aPosition.z });
	}

	void Chunk::removeBlock(ivec3 aPosition)
	{
		if (aPosition.y < 0) return;
		if (aPosition.y >= 256) throw LimitException { "Height Limit", aPosition };
		sections.at(aPosition.y / 16).removeBlock(glm::ivec3 { aPosition.x, aPosition.y % 16, aPosition.z });
	}

	void Chunk::setBlock(Block *block, ivec3 aPosition)
	{
		if (aPosition.y < 0) return;
		if (aPosition.y >= 256) throw LimitException { "Height Limit", aPosition };
		sections.at(aPosition.y / 16).setBlock(block, glm::ivec3 { aPosition.x, aPosition.y % 16, aPosition.z });
	}

}
