#pragma once

#include <array>
#include "ChunkSection.h"
#include "../WorldConstants.h"

namespace WorldComponents
{
	class ChunkMeshBuilder
	{
	public:
		explicit ChunkMeshBuilder(const ChunkSection &chunkSection);
		ChunkMesh buildMesh();

	private:
		ChunkSection chunkSection;
		ChunkMesh currentMesh {};

		void tryAddFaceToMesh(const std::array<float, 12> &face,
		                      const TextureLoader::Texture &texture,
		                      const std::array<float, 8> &textureCoords,
		                      const ivec3 &blockPosition,
							  const ivec3 &adjBlockPosition);

		static bool shouldMakeFace(const Block &adjBlock);
	};
}
