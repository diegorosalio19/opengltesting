#pragma once

#include <GLM/glm.hpp>
#include <iostream>

namespace WorldComponents
{
	using Direction = glm::vec3;

	using glm::vec3;
	using glm::ivec3;

	constexpr std::array<float, 12> topFace { 0, 1, 0, 1, 1, 0, 1, 1, 1, 0, 1, 1 };
	constexpr std::array<float, 12> bottomFace { 0, 0, 1, 1, 0, 1, 1, 0, 0, 0, 0, 0 };
	constexpr std::array<float, 12> rightFace { 1, 1, 1, 1, 1, 0, 1, 0, 0, 1, 0, 1 };
	constexpr std::array<float, 12> leftFace { 0, 1, 0, 0, 1, 1, 0, 0, 1, 0, 0, 0 };
	constexpr std::array<float, 12> backFace { 1, 1, 0, 0, 1, 0, 0, 0, 0, 1, 0, 0 };
	constexpr std::array<float, 12> frontFace { 0, 1, 1, 1, 1, 1, 1, 0, 1, 0, 0, 1 };

	constexpr std::array<unsigned int, 6> rectFaceIndices { 0, 2, 1, 0, 3, 2 };

	constexpr std::array<float, 24> cubeVertices {
			0, 0, 0, 0, 0, 1, 1, 0, 1, 1, 0, 0,
			0, 1, 0, 0, 1, 1, 1, 1, 1, 1, 1, 0
	};
	constexpr std::array<float, 24> cubeSelectedVertices {
			-0.001, -0.001, -0.001, -0.001, -0.001, 1.001, 1.001, -0.001, 1.001, 1.001, -0.001, -0.001,
			-0.001, 1.001, -0.001, -0.001, 1.001, 1.001, 1.001, 1.001, 1.001, 1.001, 1.001, -0.001
	};
	constexpr std::array<unsigned int, 36> cubeIndices {
			4, 6, 7, 4, 5, 6,
			1, 3, 2, 1, 0, 3,
			5, 2, 6, 5, 1, 2,
			7, 0, 4, 7, 3, 0,
			6, 3, 7, 6, 2, 3,
			4, 1, 5, 4, 0, 1
	};
	constexpr std::array<unsigned int, 48> cubeWireframeIndices {
			4, 7, 7, 6, 6, 5, 5, 4,
			1, 2, 2, 3, 3, 0, 0, 1,
			5, 6, 6, 2, 2, 1, 1, 5,
			7, 4, 4, 0, 0, 3, 3, 7,
			6, 7, 7, 3, 3, 2, 2, 6,
			4, 5, 5, 1, 1, 0, 0, 4
	};

	constexpr Direction TOP { 0, 1, 0 };
	constexpr Direction BOTTOM { 0, -1, 0 };
	constexpr Direction RIGHT { 1, 0, 0 };
	constexpr Direction LEFT { -1, 0, 0 };
	constexpr Direction FRONT { 0, 0, 1 };
	constexpr Direction BACK { 0, 0, -1 };

	constexpr std::array<float, 8> topFaceTextureCoords { 0.5f, 1.f, 0.625f, 1.f, 0.625f, 0.f, 0.5f, 0.f };
	constexpr std::array<float, 8> bottomFaceTextureCoords { 0.625f, 1.f, 0.75f, 1.f, 0.75f, 0.f, 0.625f, 0.f };
	constexpr std::array<float, 8> rightFaceTextureCoords { 0.0f, 1.f, 0.125f, 1.f, 0.125f, 0.f, 0.0f, 0.f };
	constexpr std::array<float, 8> leftFaceTextureCoords { 0.125f, 1.f, 0.25f, 1.f, 0.25f, 0.f, 0.125f, 0.f };
	constexpr std::array<float, 8> backFaceTextureCoords { 0.25f, 1.f, 0.375f, 1.f, 0.375f, 0.f, 0.25f, 0.f };
	constexpr std::array<float, 8> frontFaceTextureCoords { 0.375f, 1.f, 0.5f, 1.f, 0.5f, 0.f, 0.375f, 0.f };

	constexpr int CHUNK_SIZE = 16;
	constexpr int CHUNK_AREA = CHUNK_SIZE * CHUNK_SIZE;
	constexpr int CHUNK_SECTION_VOLUME = CHUNK_SIZE * CHUNK_SIZE * CHUNK_SIZE;


	inline Direction GetDirectionFromFaceCoords(std::array<float, 12> face)
	{
		if (face == topFace) return TOP;
		if (face == bottomFace) return BOTTOM;
		if (face == leftFace) return LEFT;
		if (face == rightFace) return RIGHT;
		if (face == frontFace) return FRONT;
		if (face == backFace) return BACK;
		return Direction {0};
	}

	struct LimitException : std::exception
	{
		std::string errorMessage;
		glm::ivec3 attemptedPosition;

		LimitException(const std::string &errorMessage, const ivec3 &attemptedPosition) : errorMessage(errorMessage),
		                                                                                  attemptedPosition(
				                                                                                  attemptedPosition)
		{}
	};
}