#pragma once

#include "../World.h"
#include "Generator.h"

class FlatGenerator : public Generator
{
public:
	explicit FlatGenerator(WorldComponents::World &world, int terrainHeight = 64);

	void generate(glm::ivec2 chunkPosition) override;
};
