#pragma once

#include "../World.h"
#include "Generator.h"
#include "../../../dependencies/PERLINNOISE/include/PerlinNoise.h"

class NormalGenerator : public Generator
{
public:
    explicit NormalGenerator(WorldComponents::World &world, int maxTerrainHeight = 64);

    void generate(glm::ivec2 chunkPosition) override;

private:
    siv::PerlinNoise perlinNoise;
};
