#include "NormalGenerator.h"
#include <chrono>
#include <random>

NormalGenerator::NormalGenerator(WorldComponents::World &world, int maxTerrainHeight) : Generator(world,
                                                                                                  maxTerrainHeight)
{
    perlinNoise = siv::PerlinNoise {(unsigned int) (std::chrono::system_clock::now().time_since_epoch().count())};
}

void NormalGenerator::generate(glm::ivec2 chunkPosition)
{
    if (world.chunks.find(chunkPosition) != world.chunks.end()) return;
    std::array<WorldComponents::ChunkSection, 16> sectionsArray{};

    for (int k = 0; k < WorldComponents::CHUNK_SIZE; ++k) {
        sectionsArray.at(k) = WorldComponents::ChunkSection{glm::ivec3{chunkPosition.x, k, chunkPosition.y}};
    }

    for (int i = 0; i < WorldComponents::CHUNK_SIZE; ++i) {
        for (int j = 0; j < WorldComponents::CHUNK_SIZE; ++j) {
            int value = perlinNoise.noise2D_01(chunkPosition.x * WorldComponents::CHUNK_SIZE + i / 64.f, chunkPosition.y * WorldComponents::CHUNK_SIZE + j / 64.f) * terrainHeight;
            for (int k = 0; k < value; ++k) {
                sectionsArray.at(k / WorldComponents::CHUNK_SIZE).setBlock(
                        new WorldComponents::Block{WorldComponents::BlockType::DIRT},
                        glm::ivec3{i, k % WorldComponents::CHUNK_SIZE, j});
            }
        }
    }

    world.chunks.insert(std::pair<glm::ivec2, WorldComponents::Chunk *>{chunkPosition,
                                                                        new WorldComponents::Chunk{sectionsArray,
                                                                                                   chunkPosition}});
}
