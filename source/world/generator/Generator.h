#pragma once

#include "../World.h"

class Generator
{
public:
    Generator(WorldComponents::World &world, int maxTerrainHeight) : world{world}, terrainHeight{maxTerrainHeight}
    {}

    virtual void generate(glm::ivec2 chunkPosition) = 0;

    static void fillChunkSection(WorldComponents::ChunkSection &chunkSection, int level)
    {
        for (int i = 0; i < WorldComponents::CHUNK_SIZE; ++i)
            for (int j = 0; j < level; ++j)
                for (int k = 0; k < WorldComponents::CHUNK_SIZE; ++k) {
                    chunkSection.setBlock(new WorldComponents::Block { WorldComponents::BlockType::DIRT },
                                          glm::ivec3 { i, j, k });
                }
    }

protected:
    int terrainHeight;
    WorldComponents::World &world;
};
