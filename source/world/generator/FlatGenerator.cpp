#include "FlatGenerator.h"
#include <algorithm>

FlatGenerator::FlatGenerator(WorldComponents::World &world, int aTerrainHeight) : Generator(world, aTerrainHeight)
{}

void FlatGenerator::generate(glm::ivec2 chunkPosition)
{
    if (world.chunks.find(chunkPosition) != world.chunks.end()) return;

    std::array<WorldComponents::ChunkSection, 16> sectionsArray{};

    for (int i = 0, level = terrainHeight; i < 16; ++i) {
        sectionsArray.at(i) = WorldComponents::ChunkSection{glm::ivec3{chunkPosition.x, i, chunkPosition.y}};
        fillChunkSection(sectionsArray.at(i), std::clamp(level, 0, 16));
        level -= std::clamp(level, 0, 16);
    }

    world.chunks.insert(std::pair<glm::ivec2, WorldComponents::Chunk *>{chunkPosition,
                                                                        new WorldComponents::Chunk{sectionsArray,
                                                                                                   chunkPosition}});
}


