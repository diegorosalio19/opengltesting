#pragma once

#include "GLM/glm.hpp"

struct DirectionLight {
	glm::vec3 direction;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
};

struct PointLight{
	glm::vec3 position;
	float constant;
	float linear;
	float quadratic;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
};

struct SpotLight
{
	glm::vec3 position;
	glm::vec3 direction;
	float cutOff;
	float outerCutOff;
	glm::vec3 ambient;
	glm::vec3 diffuse;
	glm::vec3 specular;
};


inline PointLight SetAttenuationByDistance(PointLight pointLight, float distance)
{
	pointLight.constant = 1.f;
	pointLight.linear = 4.5f/distance;
	pointLight.quadratic = 75.f / (distance*distance);
	return pointLight;
}