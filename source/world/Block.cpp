#include "Block.h"
namespace WorldComponents
{
	Block::Block(BlockType blockT) : blockType { blockT }
	{

	}

	bool Block::isOpaque() const
	{
		if (blockType == BlockType::DIRT) return true;
		else return false;
	}

	TextureLoader::Texture Block::getTexture()
	{
		return TextureLoader::LoadTexture("assets/block/grass_block_diffuse.png");
	}
}