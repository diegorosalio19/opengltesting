#include "World.h"

namespace WorldComponents
{

	inline glm::ivec2 getChunkPosition(const glm::ivec3 &absolutePosition)
	{
		int x, z;
		x = absolutePosition.x / 16;
		z = absolutePosition.z / 16;
		if (absolutePosition.x < 0) x = (absolutePosition.x+1) / 16 - 1;
		if (absolutePosition.z < 0) z = (absolutePosition.z+1) / 16 - 1;
		return glm::ivec2{x, z};
	}

	inline glm::ivec3 getRelativeBlockPosition(const glm::ivec3 &absolutePosition)
	{
		int x = absolutePosition.x, z = absolutePosition.z;
		if (absolutePosition.x < 0) x++;
		if (absolutePosition.z < 0) z++;
		x = std::abs(x % CHUNK_SIZE);
		z = std::abs(z % CHUNK_SIZE);
		if (absolutePosition.x < 0) x = 15 - x ;
		if (absolutePosition.z < 0) z = 15 - z ;
		return glm::ivec3{x, absolutePosition.y, z};
	}

	World::World(Camera *camera) : camera { camera }
	{

	}

	Camera *World::getCamera()
	{
		return camera;
	}

	glm::mat4 World::getProjViewMatrix(const ScreenSize &screenSize) const
	{

		return glm::perspective(
				glm::radians(camera->getFoV()),
				(float) screenSize.width / (float) screenSize.height,
				.1f, 100.f
		) * camera->getViewMatrix();
	}

	Block *World::getBlock(ivec3 aPosition)
	{
		auto chunkPair = chunks.find(glm::ivec2 { getChunkPosition(aPosition) });
		if (chunkPair == chunks.end()) return nullptr;
		return chunkPair->second->getBlock(getRelativeBlockPosition(aPosition));
	}

	void World::removeBlock(ivec3 aPosition)
	{
		auto chunkPair = chunks.find(glm::ivec2 { getChunkPosition(aPosition) });
		if (chunkPair == chunks.end()) return;
		chunkPair->second->removeBlock(getRelativeBlockPosition(aPosition));
	}

	void World::setBlock(Block *block, ivec3 aPosition)
	{
		auto chunkPair = chunks.find(glm::ivec2 { getChunkPosition(aPosition) });
		if (chunkPair == chunks.end()) return;
		chunkPair->second->setBlock(block, getRelativeBlockPosition(aPosition));
	}

    vec3 World::getPlayerPosition() const
    {
        return camera->getPos();
    }
}
