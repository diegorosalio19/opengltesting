#pragma once

#include "GLM/glm.hpp"
#include "GLM/gtc/matrix_transform.hpp"

enum class CameraMovement
{
	FORWARD, BACKWARD, LEFT, RIGHT, UP, DOWN
};

namespace CameraDefault
{
	const float YAW = -90.0f;
	const float PITCH = 0.0f;
	const float SPEED = 8.5f;
	const float SENSITIVITY = 0.1f;
	const float ZOOM = 90.0f;
};

class Camera
{
private:
	glm::vec3 front, up {}, pos {}, right {}, worldUp {};
	float yaw, pitch, movementSpeed, mouseSensitivity, zoomFactor;
public:
	explicit Camera(
			glm::vec3 position = glm::vec3 { 0.0f, 0.0f, 0.0f },
			glm::vec3 up = glm::vec3 { 0.0f, 1.0f, 0.0f },
			float yaw = CameraDefault::YAW,
			float pitch = CameraDefault::PITCH
	);
	[[nodiscard]] glm::mat4 getViewMatrix() const;
	void handleMouseMovement(float xOffset, float yOffset, bool constrainPitch = true);
	void handleMouseScroll(float yOffset);
	void handleKeyboard(CameraMovement direction, float deltaTime);
	
	void setSpeed(float speed);

	float getFoV() const;
	[[nodiscard]] glm::vec3 getPos() const;
	[[nodiscard]] glm::vec3 getFront() const;

private:
	void updateCameraVectors();
};