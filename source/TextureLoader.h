#pragma once

#include <vector>
#include <array>
#include <string>
#include <cstring>
#include <iostream>
#include "GLAD/glad.h"

#ifndef STB_IMAGE_IMPLEMENTATION

#include "../dependencies/STB_IMAGE/stb_image.h"

#endif

namespace TextureLoader
{
    struct Texture
    {
        unsigned int id;
        std::string path;
    };

    unsigned int TextureFromFile(const std::string &path);

    Texture LoadTexture(const std::string &path);

    Texture LoadTexture(const std::vector<std::string> &facePaths);

    unsigned int CubeMapTextureFromFile(const std::vector<std::string> &facePaths);
};