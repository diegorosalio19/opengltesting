#define STB_IMAGE_IMPLEMENTATION

#include "util.h"
#include "GUI/Application.h"

int main()
{
	Application display {};
	try {
		display.show();
	} catch (WorldComponents::LimitException &limitException) {
		std::cout << limitException.errorMessage;
	}
	return 0;
}

