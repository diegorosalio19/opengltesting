#include "TextureLoader.h"

namespace TextureLoader
{
	std::vector<Texture> loadedTextures;
	Texture LoadTexture(const std::string& path)
	{
		for (auto & loadedTexture : loadedTextures) {
			if (std::strcmp(loadedTexture.path.c_str(), path.c_str()) == 0){
				return loadedTexture;
			}
		}
		Texture texture {};
		texture.id = TextureFromFile(path);
		texture.path = path;
		loadedTextures.push_back(texture);
		return texture;
	}

    Texture LoadTexture(const std::vector<std::string> &facePaths)
    {
        for (auto & loadedTexture : loadedTextures) {
            if (std::strcmp(loadedTexture.path.c_str(), facePaths.front().c_str()) == 0){
                return loadedTexture;
            }
        }
        Texture texture {};
        texture.id = CubeMapTextureFromFile(facePaths);
        texture.path = facePaths.front();
        loadedTextures.push_back(texture);
        return texture;
    }

	unsigned int TextureFromFile(const std::string &path)
	{
		unsigned int texture;
		glGenTextures(1, &texture);
		int width, height, nrComponents;
		stbi_set_flip_vertically_on_load(true);
		unsigned char* data = stbi_load(path.c_str(), &width, &height, &nrComponents, 0);
		if (data) {
			int format = 0;
			if (nrComponents == 1)
				format = GL_RED;
			else if (nrComponents == 3)
				format = GL_RGB;
			else if (nrComponents == 4)
				format = GL_RGBA;

			glBindTexture(GL_TEXTURE_2D, texture);
			glTexImage2D(GL_TEXTURE_2D, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
			glGenerateMipmap(GL_TEXTURE_2D);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_REPEAT);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
			glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
		}
		else {
			std::cout << "Failed to load texture" << std::endl;
		}
		stbi_image_free(data);
		glBindTexture(GL_TEXTURE_2D, 0);
		return texture;
	}

    unsigned int CubeMapTextureFromFile(const std::vector<std::string> &facePaths)
    {
        unsigned int texture;
        glGenTextures(1, &texture);
        glBindTexture(GL_TEXTURE_CUBE_MAP, texture);

        int width, height, nrComponents;
        stbi_set_flip_vertically_on_load(true);
        for (int i = 0; i < facePaths.size(); ++i) {
            unsigned char* data = stbi_load(facePaths.at(i).c_str(), &width, &height, &nrComponents, 0);
            if (data) {
                int format = 0;
                if (nrComponents == 1)
                    format = GL_RED;
                else if (nrComponents == 3)
                    format = GL_RGB;
                else if (nrComponents == 4)
                    format = GL_RGBA;

                glTexImage2D(GL_TEXTURE_CUBE_MAP_POSITIVE_X + i, 0, format, width, height, 0, format, GL_UNSIGNED_BYTE, data);
                stbi_image_free(data);
            }
            else {
                std::cout << "Failed to load cube map texture" << std::endl;
            }
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_WRAP_R, GL_CLAMP_TO_EDGE);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
            glTexParameteri(GL_TEXTURE_CUBE_MAP, GL_TEXTURE_MAG_FILTER, GL_LINEAR);

            glBindTexture(GL_TEXTURE_CUBE_MAP, 0);
        }
    }

};