#version 330 core

layout (location = 0) in vec3 aPosition;

uniform mat4 uProjViewMatrix;

void main() {
    gl_Position = uProjViewMatrix*vec4(aPosition, 1.0);
}
