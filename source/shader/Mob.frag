#version 330

in vec3 fragPos;
//in vec3 normalVector;
in vec2 texCoords;

uniform sampler2D uTexture;

out vec4 FragColor;

void main() {
    FragColor = vec4(vec3(texture(uTexture, texCoords)), 1.0);
}
