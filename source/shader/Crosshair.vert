#version 330 core
layout (location = 0) in vec2 aPosition;

uniform mat4 uModel;
uniform mat4 uProjection;

void main()
{
    gl_Position = uProjection*uModel*vec4(aPosition, -1.0, 1.0);
    gl_Position.z = -1;
}