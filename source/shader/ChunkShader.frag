#version 330 core

struct DirLight {
    vec3 direction;
    vec3 ambient;
    vec3 diffuse;
    vec3 specular;
};

in vec3 fragPos;
in vec3 normalVector;
in vec2 texCoords;

uniform sampler2D uTexture;
uniform vec3 uEyePos;
uniform DirLight uDirLight;

out vec4 FragColor;

vec3 CalcDirLight(DirLight dirLight, vec3 normal, vec3 eyePos);

void main()
{
    vec3 norm = normalize(normalVector);
    vec3 result = CalcDirLight(uDirLight, norm, uEyePos);

    FragColor = vec4(result, (texture(uTexture, texCoords)).a);
}

vec3 CalcDirLight(DirLight light, vec3 normal, vec3 eyePos)
{
    vec3 lightDir = normalize(-light.direction);
    float diff = max(dot(lightDir, normal), 0);

    vec3 reflDir = reflect(-lightDir, normal);
    vec3 eyeDir = normalize(eyePos - fragPos);
    float spec = pow(max(dot(reflDir, eyeDir), 0), 64);

    vec3 ambient  = light.ambient  * vec3(texture(uTexture, texCoords));
    vec3 diffuse  = light.diffuse  * diff * vec3(texture(uTexture, texCoords));
    vec3 specular = light.specular * spec * vec3(texture(uTexture, texCoords));

    return (ambient + specular + diffuse);
}

