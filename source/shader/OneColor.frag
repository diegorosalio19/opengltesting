#version 330 core

uniform vec3 uColor; //TODO vec4 for alpha test

out vec4 FragColor;

void main() {
    FragColor = vec4(uColor, 1.0);
}
