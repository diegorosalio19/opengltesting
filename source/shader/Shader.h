#pragma once
#include "GLAD/glad.h"

#include <string>
#include <fstream>
#include <sstream>
#include <iostream>
#include "GLM/glm.hpp"
#include "GLM/gtc/type_ptr.hpp"

class Shader
{
public:
	unsigned int ID;
public:
	Shader(const char* vertexPath, const char* fragmentPath);
	
	void use() const;

    void setMat4f(const std::string& name, glm::mat4 mat) const;
	void setFloat(const std::string& name, float v) const;
	void setInt(const std::string& name, int v) const;
	void setVec3(const std::string &name, const glm::vec3 &vec) const;
};