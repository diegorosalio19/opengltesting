#version 330

layout (location = 0) in vec3 aPosition;
layout (location = 1) in vec2 aTexCoords;
//layout (location = 2) in vec3 aNormals;

uniform mat4 uProjViewMatrix;

out vec3 fragPos;
//out vec3 normalVector;
out vec2 texCoords;

void main() {
    gl_Position = uProjViewMatrix*vec4(aPosition, 1.0);
    texCoords = aTexCoords;
    fragPos = aPosition;
//    normalVector = aNormals;
}
