#include "RenderEngine.h"

#include <utility>
#include "../GUI/InputHandler.h"

void enableTests();

void setDirectionLightIntoShader(const DirectionLight &light, const Shader &shader);

RenderEngine::RenderEngine(const Shader &basicShader, const ScreenSize &screenSize)
		: basicShader(basicShader), screenSize(screenSize)
{
	enableTests();
}

void RenderEngine::clear()
{
	glBindFramebuffer(GL_FRAMEBUFFER, 0);
	glClearColor(0.1f, 0.1f, 0.1f, .0f);
	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT | GL_STENCIL_BUFFER_BIT);
}

void RenderEngine::drawGUI(const GUI &gui)
{
	gui.crosshair.draw();
}

void RenderEngine::drawWorld(const WorldComponents::World &world)
{
	basicShader.use();
	basicShader.setMat4f("uProjViewMatrix", world.getProjViewMatrix(screenSize));
	DirectionLight directionLight { glm::vec3 { -1 }, glm::vec3 { 0.2 }, glm::vec3 { 1 }, glm::vec3 { 1 }};
	setDirectionLightIntoShader(directionLight, basicShader);

    for (int i = -8; i < 9; ++i) {
        for (int j = -8; j < 9; ++j) {
            auto chunk = world.chunks.find(glm::ivec2{world.getPlayerPosition().x / 16 + i, world.getPlayerPosition().z / 16 + j});
            if (chunk != world.chunks.end()){
                drawChunk(*(chunk->second));
            }
        }
    }
}


void RenderEngine::drawChunk(const WorldComponents::Chunk &chunk)
{
	for (auto &section: chunk.getSections()) {
		drawChunkSectionMesh(section.getMesh());
	}
}

void RenderEngine::drawChunkSectionMesh(const WorldComponents::ChunkMesh &chunkMesh)
{
	if (!chunkMesh.getTextureCoords().empty())
		glBindTexture(GL_TEXTURE_2D, chunkMesh.getTextureCoords().begin()->first.id); // TODO modify this
	unsigned int meshVAO = chunkMesh.getVAO();
	glBindVertexArray(meshVAO);
	glDrawElements(GL_TRIANGLES, (int) chunkMesh.getIndices().size(), GL_UNSIGNED_INT, nullptr);
}

void RenderEngine::drawWireFramedCube(glm::vec3 position, float side, const WorldComponents::World &world)
{
	std::vector<float> vertices { WorldComponents::cubeSelectedVertices.begin(),
	                              WorldComponents::cubeSelectedVertices.end() };
	for (int i = 0; i < 24; i += 3) {
		vertices.at(i + 0) = vertices.at(i + 0) * side + position.x;
		vertices.at(i + 1) = vertices.at(i + 1) * side + position.y;
		vertices.at(i + 2) = vertices.at(i + 2) * side + position.z;
	}
	unsigned int VBO, EBO, VAO;
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float), vertices.data(), GL_STATIC_DRAW);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(WorldComponents::cubeWireframeIndices),
	             WorldComponents::cubeWireframeIndices.data(), GL_STATIC_DRAW);
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
	glEnableVertexAttribArray(0);
	glLineWidth(2);

	outlineShader.use();
	outlineShader.setMat4f("uProjViewMatrix", world.getProjViewMatrix(screenSize));
	outlineShader.setVec3("uColor", glm::vec3 { 0.8 });

	glDrawElements(GL_LINES, WorldComponents::cubeWireframeIndices.size(), GL_UNSIGNED_INT, nullptr);

	glDeleteBuffers(1, &EBO);
	glDeleteBuffers(1, &VBO);
	glDeleteVertexArrays(1, &VAO);
}

void RenderEngine::drawMob(const Mobs::HumanLikeMob &mob, const WorldComponents::World &world)
{
	std::vector<float> vertices { Mobs::HumanLikeBody::VERTICES.begin(), Mobs::HumanLikeBody::VERTICES.end() };
	for (int i = 0, index = 0; i < vertices.size() / 3; i++) {
		vertices.at(index++) += mob.getPosition().x;
		vertices.at(index++) += mob.getPosition().y;
		vertices.at(index++) += mob.getPosition().z;
	}
//	vertices.at(26*3+2)+=0.5;
//	vertices.at(27*3+2)+=0.5;
//	vertices.at(28*3+2)+=0.5;
//	vertices.at(29*3+2)+=0.5;
//	vertices.at(30*3+2)+=0.5;
//	vertices.at(31*3+2)+=0.5;
//	vertices.at(26*3+1)+=0.5;
//	vertices.at(27*3+1)+=0.5;
//	vertices.at(28*3+1)+=0.5;
//	vertices.at(29*3+1)+=0.5;
//	vertices.at(30*3+1)+=0.5;
//	vertices.at(31*3+1)+=0.5;
	unsigned int VBO, EBO, VAO;
	glGenBuffers(1, &VBO);
	glGenBuffers(1, &EBO);
	glGenVertexArrays(1, &VAO);
	glBindVertexArray(VAO);
	glBindBuffer(GL_ARRAY_BUFFER, VBO);
	glBufferData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float) + Mobs::HumanLikeBody::TEXTURE_COORDS.size() *
	                                                                sizeof(float), nullptr, GL_STATIC_DRAW);
	glBufferSubData(GL_ARRAY_BUFFER, 0, vertices.size() * sizeof(float), vertices.data());
	glBufferSubData(GL_ARRAY_BUFFER, vertices.size() * sizeof(float),
	                Mobs::HumanLikeBody::TEXTURE_COORDS.size() * sizeof(float),
	                Mobs::HumanLikeBody::TEXTURE_COORDS.data());
	glVertexAttribPointer(0, 3, GL_FLOAT, GL_FALSE, 3 * sizeof(float), nullptr);
	glEnableVertexAttribArray(0);
	glVertexAttribPointer(1, 2, GL_FLOAT, GL_FALSE, 2 * sizeof(float), (void*)(vertices.size() * sizeof(float)));
	glEnableVertexAttribArray(1);

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, EBO);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, sizeof(Mobs::HumanLikeBody::INDICES),
	             Mobs::HumanLikeBody::INDICES.data(), GL_STATIC_DRAW);

	mobShader.use();
	mobShader.setMat4f("uProjViewMatrix", world.getProjViewMatrix(screenSize));
	auto tid = TextureLoader::LoadTexture("assets/mobs/zombie.png").id;
	glBindTexture(GL_TEXTURE_2D, tid);

	glDrawElements(GL_TRIANGLES, Mobs::HumanLikeBody::INDICES.size(), GL_UNSIGNED_INT, nullptr);

	glDeleteBuffers(1, &EBO);
	glDeleteBuffers(1, &VBO);
	glDeleteVertexArrays(1, &VAO);
}

void enableTests()
{
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glEnable(GL_CULL_FACE);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
}

void setDirectionLightIntoShader(const DirectionLight &light, const Shader &shader)
{
	shader.use();
	shader.setVec3("uDirLight.direction", light.direction);
	shader.setVec3("uDirLight.ambient", light.ambient);
	shader.setVec3("uDirLight.diffuse", light.diffuse);
	shader.setVec3("uDirLight.specular", light.specular);
}

