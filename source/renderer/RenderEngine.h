#pragma once

#include "../shader/Shader.h"
#include "../GUI/Crosshair.h"
#include "../world/chunk/ChunkMesh.h"
#include "../world/Light.h"
#include "../world/World.h"
#include "../input/ChunkBlockSelectorManager.h"
#include "../world/mobs/HumanLikeMob.h"

struct GUI
{
	Crosshair crosshair;
};

class RenderEngine
{
public:
	RenderEngine(const Shader &basicShader, const ScreenSize &screenSize);

	static void clear();
	static void drawGUI(const GUI &gui);


	void drawWorld(const WorldComponents::World &world);
	void drawChunk(const WorldComponents::Chunk &chunk);
	void drawChunkSectionMesh(const WorldComponents::ChunkMesh &chunkMesh);
	void drawWireFramedCube(glm::vec3 position, float side, const WorldComponents::World &world);

	void drawMob(const Mobs::HumanLikeMob &mob, const WorldComponents::World &world);

private:
	Shader basicShader;
	Shader outlineShader {"shader/SelectedBlock.vert", "shader/OneColor.frag"};
	Shader mobShader {"shader/Mob.vert", "shader/Mob.frag"};
	ScreenSize screenSize;
};

