#include "Ray.h"

inline glm::vec3 normalize (glm::vec3 vector)
{
	double mag = std::sqrt(std::pow(vector.x, 2) + std::pow(vector.y, 2) + std::pow(vector.z, 2));
	return glm::vec3 {(float) vector.x / mag, (float) vector.y / mag, (float) vector.z / mag};
}

Ray::Ray(const glm::vec3 &direction, const glm::vec3 &position) : direction(direction), position(position)
{
	this->direction = normalize(direction);
}

glm::vec3 Ray::getPositionAt(float step) const
{
	return glm::vec3 {
		position.x + direction.x * step,
		position.y + direction.y * step,
		position.z + direction.z * step
	};
}
