#pragma once

#include "GLM/glm.hpp"
#include "GLM/gtx/functions.hpp"

class Ray
{
public:
	explicit Ray(const glm::vec3 &direction, const glm::vec3 &position = glm::vec3 {0});

	[[nodiscard]] glm::vec3 getPositionAt(float step) const;

private:
	glm::vec3 direction, position;
};

