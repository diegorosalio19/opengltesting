#include "ChunkBlockSelectorManager.h"
#include <algorithm>
#include <iterator>

#include "../maths/Ray.h"
#include "../util.h"

ChunkBlockSelectorManager::ChunkBlockSelectorManager(WorldComponents::World &aWorld) : world(
		aWorld)
{}

ChunkBlockSelectorManager::~ChunkBlockSelectorManager()
= default;

glm::vec3 ChunkBlockSelectorManager::getSelectedBlockPosition(glm::vec3 position, glm::vec3 direction)
{
	Ray ray { direction, position };
	for (int i = 0; i < selectionRadius / SAMPLING_SIZE; ++i) {
		try {
			glm::vec3 pos = ray.getPositionAt((float) i * SAMPLING_SIZE);
			pos = RemoveDecimal(pos);
			WorldComponents::Block *block = world.getBlock(pos);
			if (block){
				return pos;
			}
		} catch (WorldComponents::LimitException &exception) {
		}
	}
	throw NothingSelectedException {};
}

void ChunkBlockSelectorManager::setSelectionRadius(float aSelectionRadius)
{
	ChunkBlockSelectorManager::selectionRadius = aSelectionRadius;
}

WorldComponents::Direction ChunkBlockSelectorManager::getSelectedFaceDirection(glm::vec3 direction)
{
	std::vector<float> dotValues{};
	dotValues.emplace_back(glm::dot(WorldComponents::TOP, -direction));
	dotValues.emplace_back(glm::dot(WorldComponents::BOTTOM, -direction));
	dotValues.emplace_back(glm::dot(WorldComponents::RIGHT, -direction));
	dotValues.emplace_back(glm::dot(WorldComponents::LEFT, -direction));
	dotValues.emplace_back(glm::dot(WorldComponents::FRONT, -direction));
	dotValues.emplace_back(glm::dot(WorldComponents::BACK, -direction));
	auto el = std::max_element(dotValues.begin(), dotValues.end());
	int index = std::distance(dotValues.begin(), el);
	switch (index) {
		case 0: return WorldComponents::TOP;
		case 1: return WorldComponents::BOTTOM;
		case 2: return WorldComponents::RIGHT;
		case 3: return WorldComponents::LEFT;
		case 4: return WorldComponents::FRONT;
		case 5: return WorldComponents::BACK;
		default: return WorldComponents::Direction {0};
	}
}
