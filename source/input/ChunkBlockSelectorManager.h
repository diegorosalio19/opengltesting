#pragma once

#include "../world/World.h"

class ChunkBlockSelectorManager
{
public:
	explicit ChunkBlockSelectorManager(WorldComponents::World &world);

	virtual ~ChunkBlockSelectorManager();

	glm::vec3 getSelectedBlockPosition(glm::vec3 position, glm::vec3 direction);
	static WorldComponents::Direction getSelectedFaceDirection(glm::vec3 direction);

	void setSelectionRadius(float selectionRadius);

	struct NothingSelectedException : public std::exception
	{};

private:
	constexpr static const float SAMPLING_SIZE { .25f };
	WorldComponents::World &world;
	double selectionRadius { 6 };

};