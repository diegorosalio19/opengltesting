#include "Camera.h"
#include "GLM/glm.hpp"
#include "GLM/gtc/matrix_transform.hpp"
#include <iostream>

Camera::Camera(
		glm::vec3 position,
		glm::vec3 up,
		float yaw,
		float pitch
) : front { 0.0f, 0.0f, -1.0f }, movementSpeed { CameraDefault::SPEED },
    mouseSensitivity { CameraDefault::SENSITIVITY }, zoomFactor { CameraDefault::ZOOM }
{
	pos = position;
	worldUp = up;
	yaw = yaw;
	pitch = pitch;
	updateCameraVectors();
}

glm::mat4 Camera::getViewMatrix() const
{
	return glm::lookAt(pos, pos + front, up);
}

void Camera::handleMouseMovement(float xOffset, float yOffset, bool constrainPitch)
{
	xOffset *= mouseSensitivity;
	yOffset *= mouseSensitivity;

	yaw += xOffset;
	pitch += yOffset;

	if (constrainPitch) {
		if (pitch > 89.0f)
			pitch = 89.0f;
		if (pitch < -89.0f)
			pitch = -89.0f;
	}
	updateCameraVectors();
}

void Camera::handleMouseScroll(float yOffset)
{
	zoomFactor -= yOffset;
	if (zoomFactor < 1.0f)
		zoomFactor = 1.0f;
	if (zoomFactor > 90.0f)
		zoomFactor = 90.0f;
}

void Camera::handleKeyboard(CameraMovement direction, float deltaTime)
{
	float speed = movementSpeed * deltaTime;
	switch (direction) {
		case CameraMovement::FORWARD:
			pos -= speed * glm::normalize(glm::cross(right, worldUp));
			break;
		case CameraMovement::BACKWARD:
			pos += speed * glm::normalize(glm::cross(right, worldUp));
			break;
		case CameraMovement::LEFT:
			pos -= speed * right;
			break;
		case CameraMovement::RIGHT:
			pos += speed * right;
			break;
		case CameraMovement::UP:
			pos += speed * worldUp;
			break;
		case CameraMovement::DOWN:
			pos -= speed * worldUp;
			break;
		default:
			break;
	}
}

void Camera::setSpeed(float speed)
{ movementSpeed = speed; }

glm::vec3 Camera::getPos() const
{
	return pos;
}

float Camera::getFoV() const
{
	return zoomFactor;
}

void Camera::updateCameraVectors()
{
	glm::vec3 _front;
	_front.x = (float) cos(glm::radians(yaw)) * (float) cos(glm::radians(pitch));
	_front.y = (float) sin(glm::radians(pitch));
	_front.z = (float) sin(glm::radians(yaw)) * (float) cos(glm::radians(pitch));
	front = glm::normalize(_front);
	right = glm::normalize(glm::cross(front, worldUp));
	up = glm::normalize(glm::cross(right, front));
}

glm::vec3 Camera::getFront() const
{
	return front;
}
