#pragma once

#include <cmath>
#include <iostream>
#include "GLAD/glad.h"
#include "GLFW/glfw3.h"
#include "shader/Shader.h"

#include "../dependencies/STB_IMAGE/stb_image.h"


constexpr auto BLOCKS_SIZE_WIDTH = 64, BLOCKS_SIZE_HEIGHT = 256, BLOCKS_SIZE_DEPTH = 64;

struct ScreenSize{
	int width, height;
};

inline glm::vec3 RemoveDecimal(glm::vec3 vec)
{
	return glm::vec3 {std::floor(vec.x), std::floor(vec.y), std::floor(vec.z)};
}

inline glm::vec3 rgb256(int r, int g, int b)
{
	return glm::vec3 {r/256.f, g/256.f, b/256.f};
}

inline glm::vec3 rgb256(float r, float g, float b)
{
	return glm::vec3 {r/256.f, g/256.f, b/256.f};
}

inline glm::ivec3 toRgb256(float r, float g, float b)
{
	return glm::ivec3 {r*256.f, g*256.f, b*256.f};
}

inline glm::vec3 rgb100(int r, int g, int b)
{
	return glm::vec3 {r/100.f, g/100.f, b/100.f};
}

inline glm::vec4 rgba(float r, float g, float b, float a)
{
	return glm::vec4 {r/256.f, g/256.f, b/256.f, a};
}

inline glm::vec3 getArcColor(float time)
{
	auto r = static_cast<float>(std::sin(((0.f/3.f)*M_PI)*time));
	auto g = static_cast<float>(std::sin(((2.f/3.f)*M_PI)*time));
	auto b = static_cast<float>(std::sin(((4.f/3.f)*M_PI)*time));
	return {r, g, b};
}

struct NonCopyable {
	NonCopyable() = default;
	NonCopyable(const NonCopyable &) = delete;
	NonCopyable &operator=(const NonCopyable &) = delete;
};

